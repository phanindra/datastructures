package com.phanindra.datastructures.sort;


public class SelectionSort {
	public static void sort(Comparable[] a, SortOrder order) {
		for (int i = 0; i < a.length; i++) {
			int swapElement = i;
			for (int j = i + 1; j < a.length; j++) {
				if(SortOrder.ASCENDING.equals(order)){
					if (a[j].compareTo(a[swapElement]) <0) {
						swapElement = j;
					}
				} else {
					if(a[j].compareTo(a[swapElement])>0) {
						swapElement = j;
					}
				}
			}
			swap(a, i, swapElement);
		}
	}

	private static void swap(Comparable[] a, int i, int swapElement) {
		Comparable temp = a[i];
		a[i] = a[swapElement];
		a[swapElement] = temp;
	}

	public static void sort(Comparable[] a) {
		sort(a, SortOrder.ASCENDING);
	}
}
