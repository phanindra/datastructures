package com.phanindra.datastructures.sort;

public class InsertionSort {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void sort(Comparable[] a) {
		int[] h = new int[] { 1 };
		int n = a.length;
		for (int hVal : h) {
			for (int i = 1; i < n; i++) {
				for (int j = i; j > 0; j -= hVal) {
					if (a[j - hVal].compareTo(a[j]) > 0) {
						swap(a, j, hVal);
					}
				}
			}
		}

	}

	private static void swap(Comparable[] a, int j, int h) {
		Comparable temp = a[j - h];
		a[j - h] = a[j];
		a[j] = temp;
	}
}
