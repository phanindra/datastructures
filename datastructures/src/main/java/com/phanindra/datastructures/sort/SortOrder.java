package com.phanindra.datastructures.sort;

public enum SortOrder {
	ASCENDING, DESCENDING
}
