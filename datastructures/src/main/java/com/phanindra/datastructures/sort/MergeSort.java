package com.phanindra.datastructures.sort;

public class MergeSort {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Comparable[] merge(Comparable[] firstSortedArray,
			Comparable[] secondSortedArray) {
		Comparable[] output = new Comparable[firstSortedArray.length
				+ secondSortedArray.length];
		// TODO add assertions to check that two input arrays are in fact sorted
		for (int i = 0, j = 0, k = 0;;) {
			if (i == firstSortedArray.length) {
				for (int x = j; x < secondSortedArray.length; x++) {
					output[k] = secondSortedArray[x];
					k++;
				}
				break;
			}

			if (j == secondSortedArray.length) {
				for (int x = i; x < firstSortedArray.length; x++) {
					output[k] = firstSortedArray[x];
					k++;
				}
				break;
			}
			int compare = firstSortedArray[i].compareTo(secondSortedArray[j]);
			if (compare > 0) {
				output[k] = secondSortedArray[j];
				k++;
				j++;
			} else if (compare < 0) {
				output[k] = firstSortedArray[i];
				k++;
				i++;
			} else if (compare == 0) {
				output[k] = firstSortedArray[i];
				k++;
				i++;
			}
		}
		return output;
	}

}
