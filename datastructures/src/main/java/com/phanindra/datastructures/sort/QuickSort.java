package com.phanindra.datastructures.sort;

public class QuickSort {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static int partition(Comparable[] array, int p, int q) {
		Comparable pivot = array[p];
		int i = p;
		for (int j = p + 1; j <= q; j++) {
			if (array[j].compareTo(pivot) < 0) {
				i++;
				swap(array, i, j);
			}
		}
		swap(array, p, i);
		return i;
	}

	@SuppressWarnings("rawtypes")
	private static void swap(Comparable[] array, int i, int j) {
		Comparable temp = array[j];
		array[j] = array[i];
		array[i] = temp;
	}

	@SuppressWarnings("rawtypes")
	private static void sort(Comparable[] array, int p, int q) {
		if (p < q) {
			int r = partition(array, p, q);
			sort(array, p, r - 1);
			sort(array, r + 1, q);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void sort(Comparable[] array) {
		sort(array, 0, array.length - 1);
	}
}
