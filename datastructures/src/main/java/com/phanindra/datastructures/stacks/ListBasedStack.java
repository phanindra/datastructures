package com.phanindra.datastructures.stacks;

import com.phanindra.datastructures.exception.StackEmptyException;
import com.phanindra.datastructures.lists.SinglyLinkedList;

public class ListBasedStack<T> implements Stack<T> {

	private SinglyLinkedList<T> list;

	public ListBasedStack() {
		list = new SinglyLinkedList<T>();
	}

	public void push(T item) {
		list.insertHead(item);
	}

	public T pop() {
		if(list.isEmpty()) {
			throw new StackEmptyException();
		}
		T item = list.getHead().getItem();
		list.setHead(list.getHead().getNext());
		return item;
	}

	public boolean isEmpty() {
		return list.getSize() == 0;
	}

}
