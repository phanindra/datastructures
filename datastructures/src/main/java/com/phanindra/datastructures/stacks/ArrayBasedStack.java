package com.phanindra.datastructures.stacks;

import com.phanindra.datastructures.ListADT;
import com.phanindra.datastructures.lists.ArrayBasedList;

public class ArrayBasedStack<T> implements Stack<T> {

	ListADT<T> array;

	public ArrayBasedStack() {
		this.array = new ArrayBasedList<T>();
	}

	public void push(T item) {
		array.insert(item);
	}

	public T pop() {
		return array.get(array.size() - 1);
	}

	public boolean isEmpty() {
		return array.isEmpty();
	}

}
