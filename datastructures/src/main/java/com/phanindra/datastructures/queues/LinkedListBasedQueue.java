package com.phanindra.datastructures.queues;

import com.phanindra.datastructures.lists.SinglyLinkedList2;

public class LinkedListBasedQueue<T> implements Queue<T> {

	private SinglyLinkedList2<T> list;

	public LinkedListBasedQueue() {
		this.list = new SinglyLinkedList2<T>();
	}

	public void enqueue(T item) {
		list.insert(item);
	}

	public T dequeue() {
		return list.deleteHead();
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

}
