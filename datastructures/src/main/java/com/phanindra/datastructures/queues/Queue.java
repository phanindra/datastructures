package com.phanindra.datastructures.queues;

public interface Queue<T> {
	void enqueue(T item);
	T dequeue();
	boolean isEmpty();
}
