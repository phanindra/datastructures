package com.phanindra.datastructures;

public interface ListADT {
  void insert(int location, int item);
  void delete(int location);
  void set(int location, int item);
  int get(int location);
  void insert(int item);
  int size();
  void insertEnd(int item);
  void insertHead(int item);
}
