package com.phanindra.datastructures.lists;

import com.phanindra.datastructures.ListADT;

/**
 * In computer science, a dynamic array, growable array, resizable array,
 * dynamic table, mutable array, or array list is
 * a random access, variable-size list data structure that allows elements
 * to be added or removed.
 */
public class ArrayBasedList<T> implements ListADT<T> {

  private T[] array;

  /**
   * The last index of this list that contains an entry
   */
  private int lastIndex;
  
  @SuppressWarnings("unchecked")
  public ArrayBasedList() {
    array = (T[])new Object[5];
    lastIndex = -1;
  }

  /**
   * @param location The location to insert the item at
   * @param itemToInsert The item to insert
   */
  public void insert(int location, T itemToInsert) {
    if (lastIndex + 1 == array.length) {
      array = resize(array);
    }
    // Starting from the end to slide elements and make space
    for (int counter = lastIndex + 1; counter > location; counter--) {
      array[counter] = array[counter - 1];
    }
    array[location] = itemToInsert;
    lastIndex++;
  }
  
  @SuppressWarnings("unchecked")
  private T[] resize(T[] array) {
    T[] newArray = (T[]) new Object[2 * array.length];
    for (int counter = 0; counter < array.length; counter++) {
      newArray[counter] = array[counter];
    }
    return newArray;
  }

  /**
   * @param location The index of the element to be deleted
   */
  public void delete(int location) {
    for (int counter = location + 1; counter <= lastIndex; counter++) {
      array[counter - 1] = array[counter];
    }
    array[lastIndex] = null;
    lastIndex--;
  }

  public void set(int location, T item) {
    array[location] = item;
  }

  public T get(int location) {
    return array[location];
  }

  @Override
  public String toString() {
    String output = "";
    for (int counter = 0; counter < array.length; counter++) {
      output += array[counter];
    }
    return output;
  }

  public void insert(T item) {
    insert(lastIndex, item);
  }

  public int size() {
    return lastIndex + 1;
  }

  public void insertEnd(T item) {
    // TODO Auto-generated method stub

  }

public void insertHead(T item) {
	// TODO Auto-generated method stub
	
}

public boolean isEmpty() {
	return lastIndex == -1;
}

}
