package com.phanindra.datastructures.lists;

import com.google.common.base.Preconditions;
import com.phanindra.datastructures.ListADT;

public class SinglyLinkedList<T> implements ListADT<T> {
	private SinglyLinkedListNode<T> head;

	private int size;

	public SinglyLinkedListNode<T> getHead() {
		return head;
	}

	public void setHead(SinglyLinkedListNode<T> head) {
		this.head = head;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public SinglyLinkedList() {
		this.head = null;
		this.size = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	public void insert(T item) {
		SinglyLinkedListNode<T> nodeToInsert = new SinglyLinkedListNode<T>();
		nodeToInsert.setItem(item);
		if (head == null) {
			head = nodeToInsert;
		} else {
			SinglyLinkedListNode<T> startNode = head;
			while (startNode.getNext() != null) {
				startNode = startNode.getNext();
			}
			startNode.setNext(nodeToInsert);
		}
		size++;
	}

	public int size() {
		return size;
	}

	public void insert(int location, T item) {
		throw new UnsupportedOperationException("TODO");
	}

	public void delete(int location) {
		SinglyLinkedListNode<T> previousNode;
		SinglyLinkedListNode<T> nextNode;
		SinglyLinkedListNode<T> travNode = head;
		for (int counter = 0; counter < location - 1; counter++) {
			travNode = travNode.getNext();
		}
		previousNode = travNode;
		nextNode = travNode.getNext().getNext();
		previousNode.setNext(nextNode);
		size--;
	}

	public void set(int location, T item) {
		SinglyLinkedListNode<T> travNode = head;
		for (int counter = 0; counter < location; counter++) {
			if (travNode.getNext() != null) {
				travNode = travNode.getNext();
			}
		}
		travNode.setItem(item);
	}

	public T get(int location) {
		Preconditions.checkArgument(location > 0, "index cannot be negative");
		Preconditions.checkArgument(location < size,
				"index cannot be greater than size");
		SinglyLinkedListNode<T> startNode = head;
		for (int counter = 0; counter < location; counter++) {
			startNode = startNode.getNext();
		}
		return startNode.getItem();
	}

	public void insertEnd(T item) {

	}

	public void insertHead(T item) {
		SinglyLinkedListNode<T> head = this.head;
		SinglyLinkedListNode<T> node = new SinglyLinkedListNode<T>();
		node.setItem(item);
		node.setNext(head);
		setHead(node);
		size++;
	}

	public boolean isEmpty() {
		return head == null;
	}

}
