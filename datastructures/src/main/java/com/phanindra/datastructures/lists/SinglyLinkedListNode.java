package com.phanindra.datastructures.lists;

public class SinglyLinkedListNode<T> {
  private T item;

  private SinglyLinkedListNode<T> next;

  public T getItem() {
    return item;
  }

  public void setItem(T item) {
    this.item = item;
  }

  public SinglyLinkedListNode<T> getNext() {
    return next;
  }

  public void setNext(SinglyLinkedListNode<T> next) {
    this.next = next;
  }
}
