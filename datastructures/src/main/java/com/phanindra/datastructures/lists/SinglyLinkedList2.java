package com.phanindra.datastructures.lists;

public class SinglyLinkedList2<T> {
	
	private SinglyLinkedListNode<T> head;
	
	private SinglyLinkedListNode<T> tail;

	public void insert(T item) {
		SinglyLinkedListNode<T> node = new SinglyLinkedListNode<T>();
		node.setItem(item);
		node.setNext(null);
		if(tail == null) {
			tail = node;
		}
		if(isEmpty()) {
			head = node;
		}
		SinglyLinkedListNode<T> temp = tail;
		temp.setNext(node);
		tail = node;
	}
	
	public T deleteHead() {
		if(isEmpty()) {
			return null;
		}
		SinglyLinkedListNode<T> temp = head;
		head = head.getNext();
		T ret = temp.getItem();
		temp = null;
		return ret;
	}
	
	public boolean isEmpty() {
		return head == null;
	}

}
