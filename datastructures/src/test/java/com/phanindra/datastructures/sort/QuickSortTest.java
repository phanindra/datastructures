package com.phanindra.datastructures.sort;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class QuickSortTest {

	@Test
	public void testPartition() {
		Integer[] array = new Integer[] { 8, 5, 6, 4, 1, 3, 9, 2 };
		verifyPartition(array, 6);
		Integer[] array2 = new Integer[] { 6, 10, 13, 5, 8, 3, 2, 11 };
		verifyPartition(array2, 3);
	}

	@SuppressWarnings("rawtypes")
	private void verifyPartition(Comparable[] array, int expectedPartitionIndex) {
		int partitionIndex = QuickSort.partition(array, 0, array.length - 1);
		assertEquals(expectedPartitionIndex, partitionIndex);
	}

	@Test
	public void testSort() {
		Integer[] array = new Integer[] { 6, 10, 13, 5, 8, 3, 2, 11 };
		QuickSort.sort(array);
		assertArrayEquals(new Integer[] { 2, 3, 5, 6, 8, 10, 11, 13 }, array);
	}
}
