package com.phanindra.datastructures.sort;

import org.junit.Assert;
import org.junit.Test;

public class MergeSortTest {

	@Test
	@SuppressWarnings("rawtypes")
	public void testMerge() {
		Integer[] a = new Integer[] { 2, 7, 13, 20 };
		Integer[] b = new Integer[] { 1, 9, 11, 12 };
		Integer[] expected = new Integer[] { 1, 2, 7, 9, 11, 12, 13, 20 };
		MergeSort ms = new MergeSort();
		Comparable[] result = ms.merge(a, b);
		Assert.assertArrayEquals(expected, result);
	}

}
