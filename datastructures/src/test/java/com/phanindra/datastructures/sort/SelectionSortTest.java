package com.phanindra.datastructures.sort;

import static org.junit.Assert.*;

import org.junit.Test;

public class SelectionSortTest {

	@Test
	public void testAscendingSortIntegers() {
		Integer[] a = new Integer[] { 8, 5, 1, 4, 2, 6, 7, 9, 3 };
		SelectionSort.sort(a);
		assertArrayEquals(new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, a);
	}

	@Test
	public void testAlphabeticalSortStrings() {
		String[] a = new String[] { "Steven", "Tim", "Quentin", "Christopher",
				"James", "Woody", "Coen", "David", "John", "Wes" };
		SelectionSort.sort(a);
		assertArrayEquals(new String[] { "Christopher", "Coen", "David",
				"James", "John", "Quentin", "Steven", "Tim", "Wes", "Woody" },
				a);
	}

	@Test
	public void testReverseAlphabeticalSortStrings() {
		String[] a = new String[] { "Steven", "Tim", "Quentin", "Christopher",
				"James", "Woody", "Coen", "David", "John", "Wes" };
		SelectionSort.sort(a, SortOrder.DESCENDING);
		assertArrayEquals(new String[] { "Woody", "Wes", "Tim", "Steven",
				"Quentin", "John", "James", "David", "Coen", "Christopher" }, a);
	}
}
