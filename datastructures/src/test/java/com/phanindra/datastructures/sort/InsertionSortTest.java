package com.phanindra.datastructures.sort;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class InsertionSortTest {

	@Test
	public void testIntegerSort() {
		Integer[] a = new Integer[] { 7, 5, 10, 3, 8, 4, 2, 9, 6 };
		InsertionSort.sort(a);
		Integer[] expected = new Integer[] { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		assertArrayEquals(expected, a);
	}

	@Test
	public void testStringSort() {
		String[] a = new String[] { "Steven", "Tim", "Quentin", "Chris",
				"James", "Woody", "Coen", "David", "John", "Wes" };
		InsertionSort.sort(a);
		String[] expected = new String[] { "Chris", "Coen", "David", "James",
				"John", "Quentin", "Steven", "Tim", "Wes", "Woody" };
		assertArrayEquals(expected, a);
	}

}
