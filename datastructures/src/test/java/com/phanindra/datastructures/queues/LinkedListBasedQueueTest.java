package com.phanindra.datastructures.queues;

import static org.junit.Assert.*;

import org.junit.Test;

public class LinkedListBasedQueueTest {

	@Test
	public void dequeueTest() {
		Queue<Integer> q = new LinkedListBasedQueue<Integer>();
		q.enqueue(13);
		q.enqueue(9);
		q.enqueue(5);
		assertEquals(Integer.valueOf(13), q.dequeue());
	}
	
	@Test
	public void dequeueEmptyList() {
		Queue<Integer> q = new LinkedListBasedQueue<Integer>();
		assertNull(q.dequeue());
	}
}
