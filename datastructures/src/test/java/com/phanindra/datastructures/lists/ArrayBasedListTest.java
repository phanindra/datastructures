package com.phanindra.datastructures.lists;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.phanindra.datastructures.ListADT;
import com.phanindra.datastructures.lists.ArrayBasedList;

public class ArrayBasedListTest {

  @Test
  public void test() {
    ListADT<Integer> aList = new ArrayBasedList<Integer>();
    aList.insert(0, 9);
    aList.insert(1, 4);
    aList.insert(2, 5);
    aList.insert(3, 1);
    aList.insert(4, 6);
    aList.insert(5, 10);
  }

  @Test
  public void testGetValidLocation() {
    ListADT<Integer> aList = new ArrayBasedList<Integer>();
    aList.insert(0);
    aList.insert(2);
    assertEquals(Integer.valueOf(2), aList.get(1));
  }
}
