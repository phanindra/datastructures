package com.phanindra.datastructures.lists;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.phanindra.datastructures.ListADT;

public class SinglyLinkedListTest {

  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void testInsert() {
    ListADT<Integer> list = new SinglyLinkedList<Integer>();
    list.insert(2);
    list.insert(3);
    assertEquals(2, list.size());
  }

  @Test
  public void getOnListValidCase() {
    ListADT<Integer> list = new SinglyLinkedList<Integer>();
    list.insert(2);
    list.insert(5);
    list.insert(4);
    assertEquals(Integer.valueOf(5), list.get(1));
  }

  @Test
  public void getOnEmptyListThrowsException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("greater than");
    SinglyLinkedList<Integer> list = new SinglyLinkedList<Integer>();
    list.get(2);
  }

  @Test
  public void getOnInvalidLocationThrowsException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("greater than");
    ListADT<Integer> list = new SinglyLinkedList<Integer>();
    list.insert(10);
    list.insert(12);
    list.insert(23);
    list.insert(22);
    list.get(10);
  }

  @Test
  public void getWithNegativeLocation() {
    exception.expect(IllegalArgumentException.class);
    ListADT<Integer> list = new SinglyLinkedList<Integer>();
    list.get(-10);
  }

  @Test
  public void deleteAtLocation() {
    ListADT<Integer> list = new SinglyLinkedList<Integer>();
    list.insert(1);
    list.insert(3);
    list.insert(7);
    list.insert(9);
    list.insert(11);
    list.insert(13);
    assertEquals(6, list.size());
    list.delete(2);
    assertEquals(5, list.size());
  }

  @Test
  public void setAtLocation() {
    ListADT<Integer> list = new SinglyLinkedList<Integer>();
    list.insert(1);
    list.insert(3);
    list.insert(7);
    list.insert(9);
    list.insert(11);
    list.insert(13);
    list.set(3, 100);
    assertEquals(Integer.valueOf(100), list.get(3));
  }
}
