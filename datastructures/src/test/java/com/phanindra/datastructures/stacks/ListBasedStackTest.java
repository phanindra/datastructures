package com.phanindra.datastructures.stacks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.phanindra.datastructures.exception.StackEmptyException;

public class ListBasedStackTest {
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void pushPopTest() {
		Stack<Integer> s = new ListBasedStack<Integer>();
		s.push(1);
		s.push(5);
		assertEquals(Integer.valueOf(5), s.pop());
	}
	
	@Test
	public void testPopOnEmptyStack() {
	   exception.expect(StackEmptyException.class);
	   Stack<Integer> s = new ListBasedStack<Integer>();
	   s.pop();
	}
	
	@Test
	public void verifyWhetherEmpty() {
		Stack<Integer> s = new ListBasedStack<Integer>();
		assertTrue(s.isEmpty());
		Stack<Integer> stackWithElements = new ListBasedStack<Integer>();
		stackWithElements.push(1);
		assertFalse(stackWithElements.isEmpty());
		assertTrue(true);
	}
}
